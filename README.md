# virtual-dom 虚拟列表
高性能渲染长列表，只对可见区域进行渲染，对非可见区域中的数据不渲染或部分渲染。
参考 https://cloud.tencent.com/developer/article/1533206

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
